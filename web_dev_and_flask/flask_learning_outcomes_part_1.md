* How to build a CRUD app using Flask - Flask Mega Tutorial
* Understand each line of code including argument types and return types of each Flask function
* Understand the Flask workflow - When do various functions get involved?
* What happens when a user registers - Understand the GET, POST, Redirect workflow?
* What happens when a user logs in - Understand the GET, POST, redirect workflow. What cookies are set by the server?
* How is the user session tracked using Flask?
* What's a model in Flask SQLAlchemy?
* What's a DB migration? What does flask migrate do?
* Build "Add user comments to Blog Post" feature

