## Elasticsearch


<img src="https://static-www.elastic.co/v3/assets/bltefdd0b53724fa2ce/blt6ae3d6980b5fd629/5bbca1d1af3a954c36f95ed3/logo-elastic.svg" alt="drawing" width="300"/>

_________


### High Level Overview

* https://en.wikipedia.org/wiki/Elasticsearch
* https://dzone.com/articles/what-is-elasticsearch-and-how-it-can-be-useful

### Understand

* Full-text search - https://en.wikipedia.org/wiki/Full-text_search
* Inverted Index, TF-IDF
    - https://www.quora.com/What-is-inverted-index-It-is-a-well-known-fact-that-you-need-to-build-indexes-to-implement-efficient-searches-What-is-the-difference-between-index-and-inverted-index-and-how-does-one-build-inverted-index
    - https://stackoverflow.com/questions/7727686/whats-the-difference-between-an-inverted-index-and-a-plain-old-index
    - https://www.kdnuggets.com/2018/08/wtf-tf-idf.html
    - https://www.quora.com/What-is-the-intuition-behind-tf-idf-transformation-and-when-should-we-apply-the-same

* Lucene
    - https://en.wikipedia.org/wiki/Apache_Lucene

* Sharding
    - https://www.digitalocean.com/community/tutorials/understanding-database-sharding
    - https://en.wikipedia.org/wiki/Shard_(database_architecture)

* How does ES(Elasticsearch) use Lucene?
    - https://stackoverflow.com/questions/27793721/what-is-the-difference-between-lucene-and-elasticsearch

* ES architecture.
     - https://codingexplained.com/coding/elasticsearch/introduction-elasticsearch-architecture
     - https://codingexplained.com/coding/elasticsearch/understanding-sharding-in-elasticsearch
     - https://codingexplained.com/coding/elasticsearch/understanding-replication-in-elasticsearch
     - https://blog.insightdatascience.com/anatomy-of-an-elasticsearch-cluster-part-i-7ac9a13b05db
     