## Intro

Popular Python frameworks like Django and Flask do not support asynchronous/background tasks out of the box. So if you run a computationally intensive task in your HTTP handler(view) other requests will have to wait and will have high response times. 

For example, if the user makes an HTTP request to download all their data, then the application may potentially have to fetch 1000s of rows across a bunch of tables, process the data and then encode the data into JSON. This will take more than a few seconds. During this CPU intensive phase, other light-weight requests will be placed in a queue by Gunicorn/uWSGI and will suffer from high latency. 

That's why we use task queues and celery to offload computationally intensive and slow tasks such as sending large number of emails to a background worker. 


____________



### Celery tutorial - without Django


**Note**: You can use redis as both the broker and the result backend.

* http://docs.celeryproject.org/en/latest/getting-started/introduction.html
* http://docs.celeryproject.org/en/latest/getting-started/first-steps-with-celery.html
* https://www.vinta.com.br/blog/2017/celery-overview-archtecture-and-how-it-works/

### Celery + Django tutorial:

* https://simpleisbetterthancomplex.com/tutorial/2017/08/20/how-to-use-celery-with-django.html

________


### Common Checklist:

* Fix all PEP8 and flake8 errors - Applies for all projects in your Python journey!!
    - Add "translate_tabs_to_spaces" setting
    - Install pylint(VS Code) / Anaconda (Sublime)

* Authentication - Add `@login_required`, or `LoginRequiredMixin`

* Authorization / Security - Perform all operations using `request.user` instead of getting URL from ID.

* Naming - Give descriptive names to all functions and variable names. 

* try, except:

Keep logic in try-except clause to a minimum. Don't have 'bare except' clauses. Catch specific exceptions instead.

* JS - Use descriptive names.

___________

