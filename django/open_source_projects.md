## Wagtail CMS

https://wagtail.io/
https://github.com/wagtail/wagtail

## Django Packages

https://djangopackages.org/
https://github.com/djangopackages/djangopackages

## Django Oscar

http://oscarcommerce.com/
https://django-oscar.readthedocs.io/en/releases-1.6/
https://github.com/django-oscar/django-oscar

## Misago

http://misago-project.org/
https://github.com/rafalp/Misago

__________

## Bootcamp

* https://github.com/vitorfs/bootcamp

## Other projects

* https://djangopackages.org/grids/g/cms/
