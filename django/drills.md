# ORM Drill

## *Library* application

### Task - Create models for the following entities

* Book - title, price
* Category - name
* BookCategory - book_id, category_id
* BookAuthor - book_id, author_id
* Cart - user_id
* CartItem - cart_id, book_id, quantity

- A book can belong to one or more categories
- A book can be written by one or more authors (users)
- A user can create a cart
- A user can add books to a cart


* Create a Django project called `library`
* Create a Django app called `books` and add it to `INSTALLED_APPS`
* Create the above models in `books/models.py` file
* Initialize the database
* Migrate the database
_____________

### Task - Create some sample data by writing a management command

How to write a a custom management command - https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/

* Create 10 users (usernames ranging from u1 to u10)
* Create 40 books with a random price ranging between 10 to 100. (book names ranging from b1 to b40)
* Create 10 categories. (names ranging from c1 to c10)
* Add 3 random categories to each book
* Add 2 random authors(users) to each book
* Create 20 carts (created by random users)
* Add 3 cart_items to each cart

(Don't enter each row manually. Instead define a Python module which inserts random data)

______________

### Drill - ORM

Enable SQL logging:

```python
import logging 
l = logging.getLogger('django.db.backends') 
l.setLevel(logging.DEBUG) 
l.addHandler(logging.StreamHandler())                                   
```

Using the Django shell write SQL queries for the following:

(Create a module called as `queries.py` within `books` and write individual functions)

* Fetch book having id = 1
* Fetch books having ids in [1, 2, 3] in one query
* Fetch all books belonging to category c1
* How many books belong to category c2?
* Which author has authored the most number of books?
* What is the total cost of all the books?
* Which is the most expensive book?
* Fetch all the books whose price exceeds 30, ordered by price in descending order
* Delete book having id=10
* Fetch all the cart_items and their corresponding books belonging to cart with id=1 using `select_related`

______________

## Common mistakes

* Don't set `user.password` directly. Use `user.set_password` instead
* Use `Book.objects.create` instead of `book = ....; book.save()`
* When you do `b = Book(pk=1)`, a local book object will be created. But it won't be fetched from the database. Use `b = Book.objects.get(pk=1)` instead
* What's the difference between pk and id?
* Querysets are lazy
* Avoid the `n+1` query problem. Use `select_related` and `prefetch_related` instead
